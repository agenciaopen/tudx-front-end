import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PromoAutoCompleteComponent } from './promo-auto-complete.component';

describe('PromoAutoCompleteComponent', () => {
  let component: PromoAutoCompleteComponent;
  let fixture: ComponentFixture<PromoAutoCompleteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PromoAutoCompleteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PromoAutoCompleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
