import { ModalComponent } from 'ng-shared-utils';
import { Usuario } from '../model/usuario';
import { loginSocial } from '../model/loginSocial';
import { ProvedorPlataformaSocialEnum } from '../Enums/ProvedorPlataformaSocialEnum';
import { FormGroup, FormControl } from '@angular/forms';
import { AuthService, FacebookLoginProvider, SocialUser, GoogleLoginProvider } from 'angularx-social-login'
import { Component, HostListener, ViewChild, OnInit} from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  menu = false;
  menuFixo = false;
  login = false;
  erro = false;
  
  passos = true;
  emailRedeSocial = false;
  logado: boolean;
  checkPolitica= false;
  
  confirmarSenha = "";
  msgtermo = "";
  txtInputRedeSocial = "Preencha o e-mail para se cadastrar com o facebook";

  typeSenha = "password";
  typeConfirmarSenha = "password";

  usuario: SocialUser;
  formUsuario: FormGroup;

  @ViewChild('cadastro', {static: true}) cadastro: ModalComponent;
  @ViewChild('msgSucesso', {static: true}) msgSucesso: ModalComponent;

  constructor(private authService: AuthService){ }

  ngOnInit(){
    this.createForm(new Usuario());
  }

  //Craindo cadastro com redes Sociais (Facebook / Goggle)
  socialSignIn(provedor: string){
    let provedorPlataformaSocial;
    if (provedor === ProvedorPlataformaSocialEnum.Facebook){
      provedorPlataformaSocial = FacebookLoginProvider.PROVIDER_ID;
    }
    else if(provedor === ProvedorPlataformaSocialEnum.Google){
      provedorPlataformaSocial = GoogleLoginProvider.PROVIDER_ID
    }

    // Verifica se o usuario tem e-mail cadastrado, caso não tenha é aberto outro modal para que o mesmo preencha.
    this.authService.signIn(provedorPlataformaSocial).then(usuario => {
      if (usuario.email != null || usuario.email != "") { 
        this.emailRedeSocial = true;
        this.usuario = usuario;
      } else { 
        this.salvarLoginSocial(usuario);
      }
    });
  }

  // Se o usuario já tiver email cadastrado na rede social ele entra nessa função para salvar os dados
  salvarLoginSocial(socialUser: SocialUser) {
    JSON.stringify(socialUser); 
    this.reset();
    this.mensagemDeSucesso();
  }

  // Salva os dados do usuario após o preenchimento do email das redes sociais. 
  salvarLoginSocialComEmail(email){
    this.usuario.email = email;
    JSON.stringify(this.usuario);
    this.reset();
    this.mensagemDeSucesso();
  }

  checkTermos(e){
    e.target.checked ? this.checkPolitica = true : this.checkPolitica = false;
  }

  mouseEnter(){
    if (this.menu == false) this.menu = true;
  }

  mouseLeave() {
    if(this.menu == true)  this.menu = false;
  }

  openModalCadastro() {
    this.cadastro.openModal();
  }

  // Passo para o proximo passo de cadastro manual do Tudx
  nextModal() {
    this.checkPolitica ? this.passos = false : this.msgtermo = "É necessário aceitar os termos antes de prosseguir com o cadastro."
  }

  // Volta um passo no cadastro manual do Tudx
  prevModal() {
    this.passos = true;
    this.emailRedeSocial = false;
  }

  createForm(usuario: Usuario) {
    this.formUsuario = new FormGroup({
      nome: new FormControl(usuario.nome),
      genero: new FormControl(usuario.genero),
      email: new FormControl(usuario.email),
      dataNascimento: new FormControl(usuario.dataNascimento),
      celular: new FormControl(usuario.celular),
      cpf:  new FormControl(usuario.cpf),
      senha: new FormControl(usuario.senha)
    });
  }

  visualizarSenha() {
    this.typeSenha == "password" ? this.typeSenha = "text" : this.typeSenha = "password";
  }

  validaConfirmarSenha(senha) {
    senha.target.value != this.formUsuario.controls['senha'].value || senha.target.value == "" ? this.erro = true : this.erro = false;
  }

  visualizarConfirmarSenha() {
    this.typeConfirmarSenha == "password" ? this.typeConfirmarSenha = "text" : this.typeConfirmarSenha = "password";
  }

  onSubmit() {
    if(
      this.formUsuario.value.nome != "" && 
      this.formUsuario.value.email != "" && 
      this.formUsuario.value.senha != ""
    ) this.mensagemDeSucesso();

    this.reset();
  }

  reset() {
    //Resetando formulario de cadastro
    this.formUsuario.reset();
    this.confirmarSenha = "";
    this.erro = false;
    this.typeSenha = "password";
    this.passos = true;
    this.emailRedeSocial = false;
  }

  mensagemDeSucesso() {
    this.cadastro.closeModal();
    this.msgSucesso.openModal();
  }

  @HostListener('window:scroll', ['$event']) onScrollEvent($event){
    window.scrollY > 86 ? ( this.menu = false, this.menuFixo = true) : (this.menuFixo = false);
  }
}
