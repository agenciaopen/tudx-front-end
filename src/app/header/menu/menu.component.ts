import { Component, Input, OnDestroy } from '@angular/core';
import { HeaderService } from '../../services/header.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnDestroy { 

  headerMenu: any;
  ListaCategoria = new Array;
  inscricao: Subscription;

  constructor(private headerService : HeaderService){ 
    this.inscricao = this.headerService.getMenu().subscribe((response)=>{
      this.headerMenu = response;
    }); 
  }

  @Input() Toggle = false; 

  categoria(lista){
    this.ListaCategoria = lista;
  }

  ngOnDestroy() {
    this.inscricao.unsubscribe();
  }

}
