
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

import { HeaderComponent } from './header.component';
import { MenuComponent } from './menu/menu.component';

import { HeaderService } from '../services/header.service';
import { AppRoutingModule } from '../app-routing.module';
import { BuscaComponent } from './busca/busca.component';

import { NgSharedUtilsModule } from 'ng-shared-utils';
import { PromoAutoCompleteComponent } from './promo-auto-complete/promo-auto-complete.component';

@NgModule({
  declarations: [HeaderComponent, MenuComponent, BuscaComponent, PromoAutoCompleteComponent],
  imports: [
    CommonModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    NgSharedUtilsModule
  ],
  exports:[
    HeaderComponent,
    BuscaComponent
  ],
  schemas:[ CUSTOM_ELEMENTS_SCHEMA ],
  providers: [ HeaderService ]
})
export class HeaderModule { }
 