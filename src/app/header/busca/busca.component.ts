import { Component, OnInit, Input } from '@angular/core';
import { FormControl } from '@angular/forms';
import { TudxWebService } from 'src/app/services/tudx/tudx-web.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-busca',
  templateUrl: './busca.component.html',
  styleUrls: ['./busca.component.scss']
})
export class BuscaComponent implements OnInit {

  @Input() busca = false;
  placeholder = "Buscar por produtos, marcas, etc";
  opcoes = new Array;
  
  constructor(private webService: TudxWebService, private router: Router) { }

  ngOnInit() {
  }

  autoComplete(e) {
    this.webService.GetAutoComplete(e).subscribe((response: any) => {
      this.opcoes = response;
    });
  }

  keypress(value) {
    this.router.navigate(['/resultado-da-busca'], { queryParams: { q: value } });
  }
}
