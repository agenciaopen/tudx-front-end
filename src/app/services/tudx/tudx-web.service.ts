import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, tap, map } from 'rxjs/operators';
import { Produto } from '../../model/produto'
 
const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

//const apiUrl = 'http://192.168.0.126/apiproduto/v1/Produto';

@Injectable({
  providedIn: 'root'
})

export class TudxWebService {
  url = 'http://tudx01';

  constructor(private http: HttpClient) { }

  PostProduto(produto: any) {
    return this.http.post(this.url + '/apiproduto/v1/Produto', produto);
  }

  GetProduto(id){
    return this.http.get(this.url + '/apiproduto/v1/Produto/'+ id)
  }

  GetAutoComplete(nome) {
    return this.http.get(this.url + '/apiproduto/v1/Produto/autocomplete/' + nome)
  }

  // GetProduto(id: number): Observable<Produto> { 
  //   const url = `${apiUrl}/${id}`;
  //   return this.http.get<Produto>(url).pipe(
  //     tap(_ => console.log(`leu o produto id=${id}`)),
  //     catchError(this.handleError<Produto>(`getProduto id${id}`))
  //   );
  // }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      console.error(error);

      return of(result as T);
    };
  }
  
} 
