import { TestBed } from '@angular/core/testing';

import { TudxWebService } from './tudx-web.service';

describe('TudxWebService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: TudxWebService = TestBed.get(TudxWebService);
    expect(service).toBeTruthy();
  });
});
