import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class HeaderService {
  url = 'http://tudx01/ApiInstitucional/v1';

  constructor(private http: HttpClient) { }

  getMenuJson(){
    let data = [
      {
        "identificador": 1,
        "nome": "Celulares e Smartphones",
        "categoriasFilhas": [
          {
            "identificador": 2,
            "identificadorSuperior": 1,
            "nome": "acessórios para celular",
            "categoriasFilhas": [
              {
                "identificador": 3,
                "identificadorSuperior": 2,
                "nome": "bateria para celular",
                "categoriasFilhas": []
              },
              {
                "identificador": 4,
                "identificadorSuperior": 2,
                "nome": "cabo de dados para celular",
                "categoriasFilhas": []
              }
            ]
          },
          {
            "identificador": 5,
            "identificadorSuperior": 1,
            "nome": "peças para celular",
            "categoriasFilhas": [
              {
                "identificador": 6,
                "identificadorSuperior": 5,
                "nome": "display",
                "categoriasFilhas": []
              },
              {
                "identificador": 7,
                "identificadorSuperior": 5,
                "nome": "camera traseira",
                "categoriasFilhas": []
              }
            ]
          }
        ]
      },
      {
        "identificador": 8,
        "nome": "Eletrodomésticos",
        "categoriasFilhas": [
          {
            "identificador": 9,
            "identificadorSuperior": 8,
            "nome": "Churrasqueira",
            "categoriasFilhas": [
              {
                "identificador": 10,
                "identificadorSuperior": 9,
                "nome": "churrasqueira a gás",
                "categoriasFilhas": []
              }
            ]
          }
        ]
      }
    ]
    return data;
  }

  getMenu(){
    return this.http.get(`${this.url}/categoria`);
  }
  
}
