export class Usuario {
    nome: string;
    genero: number;
    email: string;
    dataNascimento: Date = null;
    celular: number;
    cpf: number;
    senha: string;
}
