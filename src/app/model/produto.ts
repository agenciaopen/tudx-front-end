export class Produto {
    _id: number;
    nome_produto: string;
    menor_preco_produto: number;
    url_produto: string;
    forma_pagamento_produto: string;
    quantidade_anuncio: number;
}
