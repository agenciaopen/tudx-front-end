export class loginSocial {
    provider: string;
    id: string;
    email: string;
    name: string;
    image: string;
    token?: string;
    idtoken?: string;
}