import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Options } from 'ng5-slider';

@Component({
  selector: 'app-filtro',
  templateUrl: './filtro.component.html',
  styleUrls: ['./filtro.component.scss']
})
export class FiltroComponent implements OnInit {

  constructor() { }

  sliderForm: FormGroup = new FormGroup({ 
    sliderControl: new FormControl([0, 5000])
  });
  options: Options = {
    floor: 0,
    ceil: 5000,
    step: 50,
    animate: false,
    hideLimitLabels: true
  };


  ngOnInit() {
  }

}
