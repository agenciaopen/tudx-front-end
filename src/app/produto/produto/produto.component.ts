import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TudxWebService } from './../../services/tudx/tudx-web.service';
import { ModelPaginator } from './../../../../projects/ng-shared-utils/src/lib/paginator/model/model-paginator';

@Component({
  selector: 'app-produto',
  templateUrl: './produto.component.html',
  styleUrls: ['./produto.component.scss']
})

export class ProdutoComponent implements OnInit, OnDestroy {

  id;
  nomeProduto;
  url;
  qtdeProdutos; //Quantidade de Produtos
  produto = new Array;
  produtoSubscription;
  produtoParams = {
    "orders": [
      {
        "column": " ",
        "sortDirection": "asc"
      }
    ],
    "filter": {
      "nome": ""
    },
    "currentPage": 0,
    "totalPage": 0,
    "totalItems": 0,
    "take": 18
  }

  pag = new ModelPaginator();

  constructor(private webService: TudxWebService, private route: ActivatedRoute, private router: Router) {}
  
  ngOnInit() {
    this.route.queryParams.subscribe(
      (queryParams: any) => {
        this.nomeProduto = queryParams['q'];
        this.createPaginator();
        this.Produtos();
      }
    )
  }

  ngOnDestroy() {
    if(this.produtoSubscription) {
      this.produtoSubscription.unsubscribe();
      console.log('UNSUBSCRIBE');
    }
  }

  createPaginator() {
    this.pag.currentPage = 1;
    this.pag.take = 18;
    this.pag.startPage = 1;
    this.pag.totalPages = 10;
  }

  TrataString(valor){
    let r = valor.toLowerCase();
    r = r.replace(new RegExp(/,/g), '');
    r = r.replace(new RegExp(/ /g), '-');
    r = r.replace(new RegExp(/[/]/g), '');
    r = r.replace(new RegExp(/\s/g),"");
    r = r.replace(new RegExp(/[àáâãäå]/g),"a");
    r = r.replace(new RegExp(/æ/g),"ae");
    r = r.replace(new RegExp(/ç/g),"c");
    r = r.replace(new RegExp(/[èéêë]/g),"e");
    r = r.replace(new RegExp(/[ìíîï]/g),"i");
    r = r.replace(new RegExp(/ñ/g),"n");                
    r = r.replace(new RegExp(/[òóôõö]/g),"o");
    r = r.replace(new RegExp(/œ/g),"oe");
    r = r.replace(new RegExp(/[ùúûü]/g),"u");
    r = r.replace(new RegExp(/[ýÿ]/g),"y");
    valor = r;
    valor = valor.replace(new RegExp(/---/g), '-');
    return valor;
  }

  Produtos() {
    this.produto = [];
    this.produtoParams.filter.nome = this.nomeProduto;
    this.produtoParams.currentPage = this.pag.currentPage;
    this.produtoParams.take = this.pag.take;

    this.produtoSubscription = this.webService.PostProduto(this.produtoParams).subscribe((response: any) => { 
      this.pag.totalItems = response.totalItems;
      this.pag.totalPages = response.totalPage;

      this.qtdeProdutos = response.totalItems;
      this.qtdeProdutos == 1 ? (this.qtdeProdutos = " 1 produto encontrado") : (this.qtdeProdutos = this.qtdeProdutos + " produtos encontrados")

      if(response.itens.length == 0) {
        this.router.navigate(['/erro'],
          {queryParams: {'q': this.nomeProduto }}
        );
      } else {
        response.itens.forEach(element => {

          this.url = this.TrataString(element.nome);
          this.produto.push(element);
        });
      }
    });
  }

  eventsGetPaginator(e) {
    this.pag.currentPage = e.currentPage;
    window.scrollTo(0, 0);
    this.Produtos();
  }
}
