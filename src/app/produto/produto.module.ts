import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MatButtonModule, MatCheckboxModule, MatPaginatorModule } from '@angular/material';
import { Ng5SliderModule } from 'ng5-slider';

import { ProdutoComponent } from './produto/produto.component';
import { ResultadoComponent } from './resultado/resultado.component';
import { FiltroComponent } from './filtro/filtro.component';

import { AppRoutingModule } from '../app-routing.module';
import { NgSharedUtilsModule } from 'ng-shared-utils';

@NgModule({
  declarations: [
    ProdutoComponent, 
    ResultadoComponent, FiltroComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule, 
    FormsModule,
    MatButtonModule, 
    MatCheckboxModule,
    MatPaginatorModule,
    Ng5SliderModule,
    AppRoutingModule,
    NgSharedUtilsModule
  ],
  schemas:[ CUSTOM_ELEMENTS_SCHEMA ],
  exports:[
    ProdutoComponent
  ]
})
export class ProdutoModule { }
