import { ToastrModule, ToastPackage } from 'ngx-toastr';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { MatButtonModule, MatCheckboxModule } from '@angular/material';
import { HttpClientModule } from '@angular/common/http';
import { SocialLoginModule, AuthServiceConfig, FacebookLoginProvider, GoogleLoginProvider } from 'angularx-social-login'

import { AppRoutingModule } from './app-routing.module';

//aplicativos
import { Ng5SliderModule } from 'ng5-slider';
import { SwiperModule } from 'ngx-swiper-wrapper';
import { SWIPER_CONFIG } from 'ngx-swiper-wrapper';
import { SwiperConfigInterface } from 'ngx-swiper-wrapper';
import { NgSharedUtilsModule } from 'ng-shared-utils';

//Componentes
import { AppComponent } from './app.component';
import { PontodeinteresseComponent } from './components/pontodeinteresse/pontodeinteresse.component';
import { PromoComponent } from './components/promo/promo.component';
import { AnunciosComponent } from './components/anuncios/anuncios.component';
import { DiferenciaisComponent } from './components/diferenciais/diferenciais.component';
import { FooterComponent } from './components/footer/footer.component';
import { DetalheProdutoComponent } from './pages/detalhe-produto/detalhe-produto/detalhe-produto.component';
import { ErroComponent } from './erro/erro.component';

//Pages
import { HomeComponent } from './pages/home/home.component';

//Module
import { HeaderModule } from './header/header.module';
import { ProdutoModule } from './produto/produto.module';

//Services

const DEFAULT_SWIPER_CONFIG: SwiperConfigInterface = {
  direction: 'horizontal',
  slidesPerView: 'auto'
};

const config = new AuthServiceConfig([
  {
    id: FacebookLoginProvider.PROVIDER_ID,
    provider: new FacebookLoginProvider('387197935543090')
  },
  {
    id: GoogleLoginProvider.PROVIDER_ID,
    provider: new GoogleLoginProvider('7807859306-8ghs9ejd7eejj0i6q34lo0oh0a9btsn6.apps.googleusercontent.com')
  }
])

export function provideConfig() {
  return config;
}

@NgModule({
  declarations: [ 
    AppComponent,
    PontodeinteresseComponent,
    PromoComponent, 
    HomeComponent,
    AnunciosComponent,
    DiferenciaisComponent,
    FooterComponent,
    DetalheProdutoComponent,
    ErroComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatButtonModule, 
    MatCheckboxModule,
    Ng5SliderModule,
    ReactiveFormsModule,
    CommonModule,
    FormsModule,
    HeaderModule,
    ProdutoModule,
    SwiperModule,
    NgSharedUtilsModule,
    SocialLoginModule,
    ToastrModule
  ],
  schemas:[ CUSTOM_ELEMENTS_SCHEMA ],
  bootstrap: [AppComponent],
  providers: [
    {
      provide: SWIPER_CONFIG,
      useValue: DEFAULT_SWIPER_CONFIG
    },
    {
      provide: AuthServiceConfig,
      useFactory: provideConfig
    },{
      provide:ToastPackage
    }
  ]
})

export class AppModule { }
