import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PontodeinteresseComponent } from './pontodeinteresse.component';

describe('PontodeinteresseComponent', () => {
  let component: PontodeinteresseComponent;
  let fixture: ComponentFixture<PontodeinteresseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PontodeinteresseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PontodeinteresseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
