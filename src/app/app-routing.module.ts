import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { ResultadoComponent } from './produto/resultado/resultado.component';
import { DetalheProdutoComponent } from './pages/detalhe-produto/detalhe-produto/detalhe-produto.component';
import { ErroComponent } from './erro/erro.component';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'resultado-da-busca', component: ResultadoComponent },
  { path: 'produto/:id/:nome', component: DetalheProdutoComponent },
  { path: 'erro', component: ErroComponent }
];
 
@NgModule({
  imports: [
    RouterModule.forRoot(
      routes,
      { 
        enableTracing: true,
        scrollPositionRestoration: 'enabled' 
      }
    )
  ],
  exports: [RouterModule] 
})

export class AppRoutingModule { }
