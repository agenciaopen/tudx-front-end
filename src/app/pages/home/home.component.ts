import { Component, ViewChild, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ModalComponent, CustomToastService, ModelToast } from 'ng-shared-utils';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  modalConfirmacaoCadastro;
  dadosToast = {} as ModelToast;

  @ViewChild('confirmacaoCadastro', {static: true}) confirmacaoCadastro: ModalComponent;

  constructor (
    private route: ActivatedRoute, 
    private router: Router,
    private openToast: CustomToastService,
    private toastr: ToastrService
  ){}

  ngOnInit() { 
    this.modalConfirmacaoCadastro = this.router.url;
    if (this.modalConfirmacaoCadastro != "/") this.confirmacaoCadastro.openModal();
  }

  testemodalAlert(){
    // this.mensage.showMensage("sucess","Cadastro realizado com sucesso!");
    this.dadosToast.title = "Titulo";
    this.dadosToast.mensage = "Teste toast";
    this.dadosToast.url = "https://p2.trrsf.com/image/fget/cf/1200/1200/filters:quality(85)/images.terra.com/2018/05/09/goku2.jpg";
    this.dadosToast.background = "#c51e1e";
    this.openToast.custom(this.dadosToast);
  }

}
