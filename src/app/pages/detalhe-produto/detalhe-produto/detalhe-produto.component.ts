import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TudxWebService } from 'src/app/services/tudx/tudx-web.service';
import { Observable } from 'rxjs';
import { Produto } from '../../../model/produto';

@Component({
  selector: 'app-detalhe-produto',
  templateUrl: './detalhe-produto.component.html',
  styleUrls: ['./detalhe-produto.component.scss']
})
export class DetalheProdutoComponent implements OnInit {
  isLoadingResults
  idProduto
  detalheProduto: Produto[];
  anunciosdoProduto = new Array;
  caracteristicasDoProduto = new Array;
  qtdeAnuncios //Quantidade de anuncios
  descricao //Descrição do produto
  categoria

  constructor(private route:ActivatedRoute, private webService:TudxWebService) { 
    this.idProduto = this.route.snapshot.params['id'];
  }

  detalhesDoProduto(){
    
    //   this.webService.GetProduto(this.idProduto).subscribe(res =>{
    //     this.detalheProduto = res;
    //     console.log(this.detalheProduto);
    //     this.isLoadingResults = false;
    //   }, err => {
    //     console.log(err);
    //     this.isLoadingResults = false;
    //   });
    // }
    this.detalheProduto = [];
    this.webService.GetProduto(this.idProduto).subscribe((response: any) => {
      this.categoria = response.categoria.nome;
      this.detalheProduto.push(response);
      this.descricao = response.descricao;
      this.qtdeAnuncios = response.anuncios.length;
      this.qtdeAnuncios == 1 ? (this.qtdeAnuncios = "1 loja confiável") : (this.qtdeAnuncios = this.qtdeAnuncios + "lojas confiáveis")
      
      response.anuncios.forEach(element => {
        this.anunciosdoProduto.push(element);
      });

      response.caracteristicas.forEach(j => {
        this.caracteristicasDoProduto.push(j);
      });
      
    });
  }

  ngOnInit() {
    this.detalhesDoProduto();
  }

}
