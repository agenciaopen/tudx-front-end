import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'nsu-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnInit {

  stateModal = false;
  
  constructor() { }

  closeModal() {
    this.stateModal = false;
  }

  openModal() {
    this.stateModal = true;
  }

  ngOnInit() {
  }

}
