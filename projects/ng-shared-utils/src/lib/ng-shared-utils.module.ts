import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

//Components
import { NgSharedUtilsComponent } from './ng-shared-utils.component';
import { PaginatorComponent } from './paginator/paginator.component';
import { AutocompleteComponent } from './autocomplete/autocomplete.component';
import { ModalComponent } from './modal/modal.component';
import { ModalAlertComponent } from './modal-alert/modal-alert.component';
import { CustomToastComponent } from './custom-toast/custom-toast.component';
import { ModelToast } from './custom-toast/model-toast';

//Services
import { CustomToastService } from './custom-toast/custom-toast.service';

//plugins
import { ToastrModule } from 'ngx-toastr';

@NgModule({
  declarations: [
    NgSharedUtilsComponent, 
    PaginatorComponent, 
    AutocompleteComponent, 
    ModalComponent, 
    ModalAlertComponent, 
    CustomToastComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule, 
    FormsModule,
    ToastrModule.forRoot()
  ],
  exports: [
    NgSharedUtilsComponent, 
    PaginatorComponent,
    AutocompleteComponent,
    ModalComponent,
    ModalAlertComponent,
    ToastrModule
  ],
  entryComponents: [CustomToastComponent],
  providers: [ CustomToastService ] 
})

export class NgSharedUtilsModule { }
