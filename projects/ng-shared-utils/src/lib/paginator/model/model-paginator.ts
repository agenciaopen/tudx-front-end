export class ModelPaginator {
    currentPage: number;
    take: number;
    totalPages: number;
    totalItems: number;
    startPage:number;
    endPage:number;
}

