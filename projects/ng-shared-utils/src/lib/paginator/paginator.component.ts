import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ModelPaginator } from './model/model-paginator';

@Component({
  selector: 'nsu-paginator',
  templateUrl: './paginator.component.html',
  styleUrls: ['./paginator.component.css']
})
export class PaginatorComponent implements OnInit {

  @Input('paginatorData') paginator: ModelPaginator;
  @Output() paginatorEvents = new EventEmitter();

  numberPages = [];

  constructor() { }

  startIndex
  endIndex
  pages = [];

  arrayThePages() {
    if(this.paginator.totalPages < 10){
      this.paginator.startPage = 1;
      this.paginator.endPage = this.paginator.totalPages;
    }else if (this.paginator.totalPages >= 10) {
      if (this.paginator.currentPage <= 6) {
        this.paginator.startPage = 1;
        this.paginator.endPage = 10;
      } else if (this.paginator.currentPage + 4 >= this.paginator.totalPages) {
        this.paginator.startPage = this.paginator.totalPages - 9;
        this.paginator.endPage = this.paginator.totalPages;
      } else {
        this.paginator.startPage = this.paginator.currentPage - 5;
        this.paginator.endPage = this.paginator.currentPage + 4;
      }
    }

    // Cálculo de índices de itens iniciais e finais
    this.startIndex = (this.paginator.currentPage - 1) * this.paginator.take;
    this.endIndex = Math.min(this.startIndex + this.paginator.take - 1, this.paginator.totalItems - 1);

    // Criar um array de páginas
    this.pages = [];
    for (let i = this.paginator.startPage; i < this.paginator.endPage + 1; i++) this.pages.push(i);

    // debugger;
  }

  nextPage(e) {
    this.paginator.currentPage++;
    if (this.paginator.currentPage > this.paginator.endPage) {
      this.paginator.currentPage = this.paginator.endPage;
      e.preventDefault();
    } else {
      this.paginatorEvents.emit(this.paginator);
      this.arrayThePages();
    }
  }

  prevPage(e) {
    this.paginator.currentPage--;
    if (this.paginator.currentPage < this.paginator.startPage) {
      this.paginator.currentPage = this.paginator.startPage;
      e.preventDefault();
    } else{
      this.paginatorEvents.emit(this.paginator);
      this.arrayThePages();
    }
  }

  indexPage(index) {
    this.paginator.currentPage = index;
    this.paginatorEvents.emit(this.paginator);
    this.arrayThePages();
  }

  ngOnInit() {
    this.arrayThePages();
  }

}
