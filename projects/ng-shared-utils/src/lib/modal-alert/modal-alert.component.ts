import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'nsu-modal-alert',
  templateUrl: './modal-alert.component.html',
  styleUrls: ['./modal-alert.component.scss']
})
export class ModalAlertComponent implements OnInit {
  
  stateModal = false;
  stateAnimation = false;
  classAlert;
  animateOpenModal = "modal-alert animated bounceInDown ";
  animateCloseModal = "modal-alert animated faster fadeOut ";
  tiposAlert = ['sucess', 'error', 'mensage', 'notification', 'custom'];

  img = "";

  @Input() txtAlert: string;
  @Input() tipoDeMsg: string;
  @Input() custom;
  @Input() tempo;
  @Output() Mensagem = new EventEmitter(); 

  constructor() { }

  ngOnInit() {
    // this.tiposAlert.forEach(i => {
    //   if (this.tipoDeMsg == i) {
    //     this.classAlert = this.tipoDeMsg; 
    //   }
    // });
    // this.img = this.custom[1];
    
  }

  closeModal() {
    this.stateAnimation = false;
    setTimeout(() => {
      this.stateModal = false;
    },500);
  }

  openModal() {
    this.stateModal = true;
    this.stateAnimation = true;

    setTimeout(() => {
      this.closeModal();
    },this.tempo);
  }

  alerta(e){
    alert(e);
  }

  showMensage(tipo, mensagem) {

    this.tipoDeMsg = tipo;
    this.txtAlert = mensagem;
    this.custom = [];

    this.tiposAlert.forEach(i => {
      if (this.tipoDeMsg == i) {
        this.classAlert = this.tipoDeMsg; 
      }
    });
    this.img = this.custom[1];

    this.stateModal = true;
    this.stateAnimation = true;

    setTimeout(() => {
      this.closeModal();
    }, 4000);
  }

  dadosDaMensagem() {
    
  }

}
