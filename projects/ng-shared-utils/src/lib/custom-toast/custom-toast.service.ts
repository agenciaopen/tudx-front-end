import { Injectable } from '@angular/core';
import { GlobalConfig, ToastrService } from 'ngx-toastr';
import { CustomToastComponent } from './custom-toast.component';
import { ModelToast } from './model-toast';

@Injectable({
  providedIn: 'root'
})

export class CustomToastService {

  options: GlobalConfig;
  private lastInserted: number[] = [];


  constructor(protected toastrService: ToastrService) {
    this.options = this.toastrService.toastrConfig;
  }

  custom(custom: ModelToast) { 
    this.options.toastComponent = CustomToastComponent;
    this.options.toastClass = 'pinktoast';
    this.options.closeButton = true;
    this.options.background = custom.background;
    this.options.url = custom.url;
    const inserted = this.toastrService.show(custom.mensage, custom.title, this.options);
    document.getElementById('background').parentElement.style.backgroundColor = custom.background;
    if (inserted && inserted.toastId) { 
      this.lastInserted.push(inserted.toastId);
    }
    return inserted
  }
}
