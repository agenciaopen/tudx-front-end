import { Component, OnInit, Input } from '@angular/core';
import { Toast, ToastrService, ToastPackage } from 'ngx-toastr';
import { ModelToast } from './model-toast';

import {
  animate,
  keyframes,
  state,
  style,
  transition,
  trigger
} from '@angular/animations';

@Component({
  selector: 'nsu-custom-toast',
  styles: [`
    :host {
      position: relative;
      overflow: hidden;
      margin: 0 0 6px;
      padding: 20px;
      width: 300px;
      border-radius: 3px 3px 3px 3px;
      color: #FFFFFF;
      pointer-events: all;
      cursor: pointer;
      background:#000;
      display:block;
    }
    .col-9 {
      display: flex;
      align-items: center;
    }
    .img {
      width: 40px;
      height: 40px;
      margin-right: 15px;
      display: block;
    }
    .img img {
      border-radius: 50%;
    }
    .btn {
      -webkit-backface-visibility: hidden;
      -webkit-transform: translateZ(0);
      border:none;
    }
    .btn:hover {
      background:none;
    }
  `],
  template: `
  <div id="background" class="row" [style.display]="state.value === 'inactive' ? 'none' : ''">
    <div class="col-9">
      <div class="img">
          <img [src]="url" alt="">
      </div>
      <div>
        <div *ngIf="title" [class]="options.titleClass" [attr.aria-label]="title">
          {{ title }}
        </div>
        <div *ngIf="message && options.enableHtml" role="alert" aria-live="polite"
          [class]="options.messageClass" [innerHTML]="message">
        </div>
        <div *ngIf="message && !options.enableHtml" role="alert" aria-live="polite"
          [class]="options.messageClass" [attr.aria-label]="message">
          {{ message }}
        </div>
      </div>
    </div>
    <div class="col-3 text-right">
      <a *ngIf="options.closeButton" (click)="remove()" class="btn btn-sm">
        <i class="icon-cancel"></i>
      </a>
    </div>
  </div>
  `,
  animations: [
    trigger('flyInOut', [
      state('inactive', style({
        opacity: 0,
      })),
      transition('inactive => active', animate('400ms ease-out', keyframes([
        style({
          transform: 'translate3d(100%, 0, 0) skewX(-30deg)',
          opacity: 0,
        }),
        style({
          transform: 'skewX(20deg)',
          opacity: 1,
        }),
        style({
          transform: 'skewX(-5deg)',
          opacity: 1,
        }),
        style({
          transform: 'none',
          opacity: 1,
        }),
      ]))),
      transition('active => removed', animate('400ms ease-out', keyframes([
        style({
          opacity: 1,
        }),
        style({
          transform: 'translate3d(100%, 0, 0) skewX(30deg)',
          opacity: 0,
        }),
      ]))),
    ]),
  ],
  preserveWhitespaces: false,
})

export class CustomToastComponent extends Toast {

  url: string;
  background: string;

  constructor(private service: ToastrService, private packages: ToastPackage) {
    super(service, packages)
    this.url = service.toastrConfig.url;
    this.background = service.toastrConfig.background;
  }

}
