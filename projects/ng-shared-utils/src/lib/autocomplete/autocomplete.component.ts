import { Component, OnInit, Input, Output, EventEmitter, HostListener, OnDestroy } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Subscription } from 'rxjs';

@Component({
  selector: 'nsu-autocomplete',
  templateUrl: './autocomplete.component.html',
  styleUrls: ['./autocomplete.component.scss']
})

export class AutocompleteComponent implements OnInit, OnDestroy {

  control = new FormControl();
  count = 0;
  emit = true;
  valor = "";
  inputValue: string;
  inputSubscription: Subscription;

  @Input() opcoes = [];
  @Input() scroll:Boolean;
  @Input() placeholder:string;
  @Output() onValueChanged = new EventEmitter();
  @Output('enter') keypress = new EventEmitter();

  constructor() { }

  ngOnInit() {
    this.changeValue();
  }

  ngOnDestroy() {
    this.inputSubscription.unsubscribe();
  }

  changeValue() {
    this.inputSubscription = this.control.valueChanges.subscribe((res:any) => {
      if(this.emit){
        if (res != []) {
          this.onValueChanged.emit(res); this.valor = res;
        } else {
          this.opcoes = [];
          this.valor = "";
        }
      }
    });
  }

  moveDown() {
    this.emit = false;
    this.count >= this.opcoes.length ? this.count = this.opcoes.length : this.count++;
    this.inputValue = this.opcoes[this.count -1]; 
    this.control.setValue(this.inputValue);
    
  }

  moveUp() {
    this.emit = false;
    this.count < 1 ? this.count = 1 : this.count--;
    
    if(this.count == 0) { 
      this.control.setValue(this.valor); 
      this.inputValue = "";
      this.emit = true;
    } else {
      this.inputValue = this.opcoes[this.count -1]; 
      this.control.setValue(this.inputValue);
    }
  }

  enter() {
    this.keypress.emit(this.control.value);
    this.opcoes = [];
    this.resetValue();
  }

  pesquisar(item) {
    this.opcoes = [];
    this.emit = false;
    this.keypress.emit(item);
    this.control.setValue(item);
  }

  resetValue() {
    this.count = 0;
    this.emit = true;
  }

  reset() {
    this.inputValue = " "; 
    this.control.reset();
    this.valor = "";
    this.opcoes = [];
    this.count = 0;
    this.emit = true;
  }

  @HostListener('window:scroll', ['$event']) onScrollEvent($event){
    if(window.scrollY > 86) this.opcoes = [];
  }

}
